#include"Sort.h"
void PrintArray(int* a, int n)//打印
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
void BubbleSort(int* a, int n)//冒泡排序
{
	for (int j = 0; j < n; j++)
	{
		bool exchange = false;
		for (int i = 1; i < n - j; i++)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = true;
			}
		}

		if (exchange == false)
			break;
	}
}
//三数取中
int GetMidi(int* a, int begin, int end)
{
	int midi = (begin + end) / 2;
	// begin midi end 三个数选中位数
	if (a[begin] < a[midi])
	{
		if (a[midi] < a[end])
			return midi;
		else if (a[begin] > a[end])
			return begin;
		else
			return end;
	}
	else // a[begin] > a[midi]
	{
		if (a[midi] > a[end])
			return midi;
		else if (a[begin] < a[end])
			return begin;
		else
			return end;
	}
}
//小区间优化快排
//void Quicksortlittle(int* a, int begin, int end)
//{
//	{
//			if (begin >= end)
//				return;
//		
//			if (end - begin + 1 <= 10)
//			{
//				InsertSort(a + begin, end - begin + 1);
//			}
//			else
//			{
//				int midi = GetMidi(a, begin, end);
//				Swap(&a[midi], &a[begin]);
//		
//				int left = begin, right = end;
//				int keyi = begin;
//		
//				while (left < right)
//				{
//					// 右边找小
//					while (left < right && a[right] >= a[keyi])
//					{
//						--right;
//					}
//		
//					// 左边找大
//					while (left < right && a[left] <= a[keyi])
//					{
//						++left;
//					}
//		
//					Swap(&a[left], &a[right]);
//				}
//		
//				Swap(&a[left], &a[keyi]);
//				keyi = left;
//		
//				// [begin, keyi-1] keyi [keyi+1, end]
//				QuickSort(a, begin, keyi - 1);
//				QuickSort(a, keyi + 1, end);
//}
//1. hoare版本
//int QuickSort1(int* a, int begin, int end)
//{
//	int midi = GetMidi(a, begin, end);
//	Swap(&a[midi], &a[begin]);
//
//	int left = begin, right = end;
//	int keyi = begin;
//
//	while (left < right)
//	{
//		// 右边找小
//		while (left < right && a[right] >= a[keyi])
//		{
//			--right;
//		}
//
//		// 左边找大
//		while (left < right && a[left] <= a[keyi])
//		{
//			++left;
//		}
//
//		Swap(&a[left], &a[right]);
//	}
//
//	Swap(&a[left], &a[keyi]);
//
//	return left;
//}
int QuickSort1(int* a, int begin, int end)
{
	int midi = GetMidi(a, begin, end);
	Swap(&a[midi], &a[begin]);

	int left = begin, right = end;
	int keyi = begin;

	while (left < right)
	{
		// 右边找小
		while (left < right && a[right] >= a[keyi])
		{
			--right;
		}

		// 左边找大
		while (left < right && a[left] <= a[keyi])
		{
			++left;
		}

		if (left < right)
		{
			Swap(&a[left], &a[right]);
		}
	}

	Swap(&a[keyi], &a[left]);

	return left;
}
//2.挖坑法
int QuickSort2(int* a, int begin, int end)
{
	int midi = GetMidi(a, begin, end);
	Swap(&a[midi], &a[begin]);

	int key = a[begin];
	int hole = begin;
	while (begin < end)
	{
		// 右边找小，填到左边的坑
		while (begin < end && a[end] >= key)
		{
			--end;
		}

		a[hole] = a[end];
		hole = end;

		// 左边找大，填到右边的坑
		while (begin < end && a[begin] <= key)
		{
			++begin;
		}

		a[hole] = a[begin];
		hole = begin;
	}

	a[hole] = key;
	return hole;
}
//3.前后指针版本
int QuickSort3(int* a, int begin, int end)
{
	int midi = GetMidi(a, begin, end);
	Swap(&a[midi], &a[begin]);
	int keyi = begin;

	int prev = begin;
	int cur = prev + 1;
	while (cur <= end)
	{
		if (a[cur] < a[keyi] && ++prev != cur)
			Swap(&a[prev], &a[cur]);

		++cur;
	}

	Swap(&a[prev], &a[keyi]);
	keyi = prev;
	return keyi;
}

void QuickSort(int* a, int begin, int end)
{
	if (begin >= end)
		return;

	int keyi = partSort(a, begin, end);
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);
}
