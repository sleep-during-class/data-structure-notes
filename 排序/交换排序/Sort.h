#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

void PrintArray(int* a, int n);//打印
void BubbleSort(int* a, int n);//冒泡排序
//三数取中
int GetMidi(int* a, int begin, int end);
//1. hoare版本
int QuickSort1(int* a, int begin, int end);
//2.挖坑法
int  QuickSort2(int* a, int begin, int end);
//3.前后指针版本
int  QuickSort3(int* a, int begin, int end);
