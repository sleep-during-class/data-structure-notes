#include"Sort.h"
void TestQuickSortNonR()
{
	
	int a[] = { 6,1,2,6,7,9,3,4,6,10,8 };
	PrintArray(a, sizeof(a) / sizeof(int));
	QuickSortNonR(a, 0, sizeof(a) / sizeof(int) - 1);
	PrintArray(a, sizeof(a) / sizeof(int));
}

void TestMergeSortNonR()
{
	
	int a[] = { 10,8,7,1,3,9,4,2,9,10,1,1,2,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	MergeSortNonR(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
}

int main()
{
	//TestQuickSortNonR();
	TestMergeSortNonR();
	return 0;
}
