#include"Sort.h"
// 引用到的函数
int PartSort3(int* a, int left, int right)
{
	int key = a[left];
	while (left < right)
	{
		while (left < right && a[right] >= key)
			--right;
		a[left] = a[right];
		while (left < right && a[left] <= key)
			++left;
		a[right] = a[left];
	}
	a[left] = key;
	return left;
}
void STInit(ST* s)//入栈
{
	s->data = NULL;
	s->top = -1;
	s->capacity = 0;
}

void STPush(ST* s, int val)//出栈
{
	if (s->top == s->capacity - 1)
	{
		int newCapacity = s->capacity == 0 ? 4 : s->capacity * 2;
		int* newData = (int*)realloc(s->data, newCapacity * sizeof(int));
		if (newData == NULL)
		{
			// 内存分配失败处理
			return;
		}
		s->data = newData;
		s->capacity = newCapacity;
	}
	s->data[++s->top] = val;
}

void STPop(ST* s)//用于弹出栈顶元素，即将top的值减1，表示栈顶位置往下移动一位。
{
	if (s->top >= 0)
		--s->top;
}

int STTop(ST* s)//于取栈顶元素的值
{
	if (s->top >= 0)
		return s->data[s->top];
	return -1; // 栈为空时的处理，这里返回-1表示栈为空
}

int STEmpty(ST* s)//判断栈是否为空
{
	return s->top == -1;
}

void STDestroy(ST* s)//销毁栈
{
	free(s->data);
	s->data = NULL;
	s->top = -1;
	s->capacity = 0;
}




void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void QuickSortNonR(int* a, int begin, int end)
{
	ST s;
	STInit(&s);
	STPush(&s, end);
	STPush(&s, begin);

	while (!STEmpty(&s))
	{
		int left = STTop(&s);
		STPop(&s);
		int right = STTop(&s);
		STPop(&s);

		int keyi = PartSort3(a, left, right);
		// [left, keyi-1] keyi [keyi+1, right]
		if (left < keyi - 1)
		{
			STPush(&s, keyi - 1);
			STPush(&s, left);
		}

		if (keyi + 1 < right)
		{
			STPush(&s, right);
			STPush(&s, keyi + 1);
		}
	}

	STDestroy(&s);
}



void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	int gap = 1;
	while (gap < n)
	{
		for (size_t i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			
			if (end1 >= n || begin2 >= n)
			{
				break;
			}

			if (end2 >= n)
			{
				end2 = n - 1;
			}

			int j = begin1;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[j++] = a[begin1++];
				}
				else
				{
					tmp[j++] = a[begin2++];
				}
			}

			while (begin1 <= end1)
			{
				tmp[j++] = a[begin1++];
			}

			while (begin2 <= end2)
			{
				tmp[j++] = a[begin2++];
			}

			memcpy(a + i, tmp + i, sizeof(int) * (end2 - i + 1));
		}

		gap *= 2;
	}


	free(tmp);
}