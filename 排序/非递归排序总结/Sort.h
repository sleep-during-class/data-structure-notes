#pragma once
#include<stdio.h>
#include<stdlib.h>
typedef struct Stack//创建一个栈
{
    int* data;//存储栈中的元素的数组指针
    int top;//栈顶元素在数组中的索引位置
    int capacity;//栈的容量，即数组的大小
} ST;


void QuickSortNonR(int* a, int begin, int end);

void MergeSortNonR(int* a, int n);