
#include"Sort.h"
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void _MergeSort(int* a, int begin, int end, int* tmp)
{

	//划分
	if (begin >= end)//只有一个元素不用划分
		return;

	int mid = (begin + end) / 2;//首尾下标相加除2得到中间点下标

	_MergeSort(a, begin, mid, tmp);//递归划分左半区域
	_MergeSort(a, mid + 1, end, tmp);//递归划分右半区域

	// [begin, mid][mid+1, end]归并

	int begin1 = begin, end1 = mid;//标记左半区第一个未排序的元素以及最后一个元素
	int begin2 = mid + 1, end2 = end;//标记右半区第一个未排序的元素以及最后一个元素
	int i = begin;//临时数组下标
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])//左半区第一个未排序的元素小于右半区第一个未排序的元素
		{
			tmp[i++] = a[begin1++];
		}
		else
		{
			tmp[i++] = a[begin2++];//右半区第一个未排序的元素小于左半区第一个未排序的元素
		}
	}

	//合并左半区剩余元素
	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}
	//合并右半区剩余元素
	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}
	//把临时数组中合并后的元素拷贝回原数组
	memcpy(a + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

//归并排序入口
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);//开辟一个辅助数组
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
}





