#pragma once

#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<time.h>

void PrintArray(int* a, int n);
void MergeSort(int* a, int n);

