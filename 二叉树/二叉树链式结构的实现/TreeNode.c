#include"TreeNode.h"
TreeNode* BuyTree(int x)//初始化一个新的节点
{
	TreeNode* node = (TreeNode*)malloc(sizeof(TreeNode));
	assert(node);
	node->data = x;
	node->left = NULL;
	node->right = NULL;
	return node;
}
TreeNode* CreateTree()//构建一棵树
{
	TreeNode* node1 = BuyTree(1);
	TreeNode* node2 = BuyTree(2);
	TreeNode* node3 = BuyTree(3);
	TreeNode* node4 = BuyTree(4);
	TreeNode* node5 = BuyTree(5);
	TreeNode* node6 = BuyTree(6);
	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node4->left = node5;
	node4->right = node6;
	return node1;
}
TreeNode* CreateTree();//构建一棵树
// 二叉树前序遍历
void PreOrder(TreeNode* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}
// 二叉树中序遍历
void InOrder(TreeNode* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}
// 二叉树后序遍历
void PostOrder(TreeNode* root) 
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}
int TreeSize(TreeNode* root)//二叉树树结点个数
{
	return root == NULL ? 0 :
		TreeSize(root->left) + TreeSize(root->right) + 1;
}
//叶子节点个数
int TreeLeafSize(TreeNode* root)
{
	if (root == NULL)
		return 0;
	if (root->left == NULL
		&& root->right == NULL)
		return 1;
	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}
//求树的高度
int TreeHeight(TreeNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	return fmax(TreeHeight(root->left), TreeHeight(root->right))+1;
}


//求k层节点个数
int TreeLevelk(TreeNode* root, BTDataType  k)
{
	assert(k > 0);
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return TreeLevelk(root->left, k - 1) + TreeLevelk(root->right, k - 1);

}
//二叉树查找值为x的节点
TreeNode* TreeFind(TreeNode* root ,BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	TreeNode* ret1  = TreeFind(root->left,x);
	if (ret1)
	{
		return ret1;
	}
	TreeNode* ret2 = TreeFind(root->left, x);
	if (ret2)
	{
		return ret2;
	}
	return NULL;
}

// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
TreeNode* TreeCreate(char* a, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}

	TreeNode* root = (TreeNode*)malloc(sizeof(TreeNode));
	if (root == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	root->data = a[(*pi)++];

	root->left = TreeCreate(a, pi);
	root->right = TreeCreate(a, pi);
	return root;
}

// 二叉树销毁
void BinaryTreeDestory(TreeNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreeDestory(root->left);
	BinaryTreeDestory(root->right);
	free(root);
}