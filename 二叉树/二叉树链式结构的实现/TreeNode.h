#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<math.h>
typedef int BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}TreeNode;

TreeNode* BuyTree(int x);//初始化一个新的节点
TreeNode* CreateTree();//构建一棵树
// 二叉树前序遍历
void PreOrder(TreeNode* root);
// 二叉树中序遍历
void InOrder(TreeNode* root);
// 二叉树后序遍历
void PostOrder(TreeNode* root);
//二叉树树结点个数
int TreeSize(TreeNode* root);
//叶子节点个数
int TreeLeafSize(TreeNode* root);
//求树的高度
int TreeHeight(TreeNode* root);
//求k层节点个数
int TreeLevelk(TreeNode* root, int k);
//二叉树查找值为x的节点
TreeNode * TreeFind(TreeNode * root, BTDataType x);
// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
TreeNode* TreeCreate(char* a, int* pi);
// 二叉树销毁
void BinaryTreeDestory(TreeNode* root);
// 判断二叉树是否是完全二叉树
int BinaryTreeComplete(TreeNode* root);