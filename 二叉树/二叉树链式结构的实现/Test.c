#include"TreeNode.h"
int main()
{
	TreeNode* root = CreateTree();
	PreOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");
	printf("%d ", TreeSize(root));
	printf("\n");
	printf("%d ", TreeLeafSize(root));
	printf("\n");
	printf("%d ", TreeHeight(root));
	printf("\n");
	printf("%d ", TreeLevelk(root,3));
	printf("\n");
	printf("%d ", TreeFind(root, 3)->data);
	printf("\n");
	 BinaryTreeDestory(root);
	 root = 0;
	 printf("\n");
	return 0;
}