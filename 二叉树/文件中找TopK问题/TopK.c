#include <stdio.h>
#include <stdlib.h>

void PrintTopK(int k)
{
    const char* file = "data.txt";
    FILE* fin = fopen(file , "r");
    if (fin == NULL)
    {
        perror("fopen error");
        return;
    }

    int* numbers = (int*)malloc(k * sizeof(int));
    if (numbers == NULL)
    {
        perror("malloc error");
        fclose(fin);
        return;
    }

    for (int i = 0; i < k; ++i)
    {
        if (fscanf(fin, "%d", &numbers[i]) == EOF)
        {
            perror("fscanf error");
            fclose(fin);
            free(numbers);
            return;
        }
    }

    for (int i = k; ; ++i)
    {
        int x;
        if (fscanf(fin, "%d", &x) == EOF)
            break;

        int min_index = 0;
        for (int j = 1; j < k; ++j)
        {
            if (numbers[j] < numbers[min_index])
                min_index = j;
        }

        if (x > numbers[min_index])
            numbers[min_index] = x;
    }

    printf("Top %d numbers:\n", k);
    for (int i = 0; i < k; ++i)
    {
        printf("%d\n", numbers[i]);
    }

    fclose(fin);
    free(numbers);
}



