#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<time.h>
typedef int HPDataTyp;
typedef struct Heap
{
	HPDataTyp * a;
int size;
int capacity;
} Hp;
void adjustup(HPDataTyp* a, int child);
void adjustdown(HPDataTyp* a, int size,int parent);
void Swap(HPDataTyp* p1, HPDataTyp* p2);
void heapSort(int* a, int);
void CreateData();
void AdjustDown(HPDataTyp* a, int size, int root);
void HeapSort(int* a, int n);