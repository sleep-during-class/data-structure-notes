#include"Heap.h"
void Swap(HPDataTyp* p1, HPDataTyp* p2)
{
	HPDataTyp temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}
void adjustup(HPDataTyp* a, int child)
{
	int parent = (child  - 1)/2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
void adjustdown(HPDataTyp* a, int size, int parent)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		if (child + 1 < size && a[child ] < a[child+1])
		{
			++child;
		}
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 +1;
		}
		else
		{
			break;
		}
	}
}
void heapSort(int* a, int n)//升序
{
	for (int i = 1; i <n; i++)
	{
		adjustup(a, i);
	}
	int end = n - 1;
	while (end > 0)
	{
		 Swap(&a[0], &a[end]);
		adjustdown(a, end, 0);
		--end;
	}
}
//void heapSort(int* a, int n)//升序
//{
//	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
//			{
//				adjustdown(a, n, i);
//			}
//	int end = n - 1;
//	while (end > 0)
//	{
//		 Swap(&a[0], &a[end]);
//		adjustdown(a, end, 0);
//		--end;
//	}
//}
//void AdjustDown(HPDataTyp* a, int size, int root)
//{
//	int parent = root;
//	int child = 2 * parent + 1;
//	//假设左孩子为较小的孩子
//	while (child < size)
//	{
//
//		if (child + 1 < size && a[child] > a[child + 1])
//		{
//			child++;
//		}
//		//如果右孩子存在，且右孩子小于左孩子，则假设不成立，右孩子为较小的孩子
//
//		if (a[parent] > a[child])
//		{
//			Swap(&a[child], &a[parent]);
//			//交换
//			parent = child;
//			child = 2 * parent + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
//
//void HeapSort(int* a, int n)
//{
//	//建小堆排降序
//	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
//	{
//		// 向下调整算法
//		adjustdown(a, n, i);
//	}
//	int end = n - 1;
//	while (end > 0)
//	{
//		Swap(&a[0], &a[end]);
//		adjustdown(a, end, 0);
//		end--;
//	}
//}

//void CreateData()
//{
//	int n= 10000000;
//	sprand(time(0));
//	const char* file = "data.tet";
//	FILE* fin = ropen(file, "w");
//	if (fin == NULL)
//	{
//		perror("fropen");
//		return;
//	}
//	for (int i = 0; i < n; ++i)
//	{
//		int x = (rand() + i)%10000000;
//		frinbtf(fin, "%d\n", x);
//	}
//	fclose(fin);
//}
//
//
//void PrintTopk(const char* file, int k)
//{
//	FILE* fout = fopen(file, "r");
//	if (fout == NULL)
//	{
//		perror("malloc fail");
//		return ;
//	}
//	int* minheap = (int *)malloc(sizeof (int) *k);
//	if (minheap == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//	for (int i = 0; i < k; i++)
//	{
//		fcanf(fout, "%d", &minheap[i]);
//		adjustup(minheap, i);
//	}
//	int x = 0;
//	while (fscanf(fout, "%d", &x) != EOF)
//	{
//		if (x > minheap[0])
//		{
//			minheap[0] = x;
//			adjustdown(minheap, k, 0);
//		}
//	}
//	for (int i = 0; i < k; i++)
//	{
//		printf("%d ", minheap[i]);
//	}
//	printf("\n");
//	free(minheap);
//	fclose(fout);
//}
