#include"Heap.h"
//交换数据
void Swap(HPDataType *p1, HPDataType *p2)
{
    HPDataType temp = *p1;
    *p1 =*p2;
    *p2 = temp;
}
//向上调整堆
void AdjudtUp(HPDataType* a, int child)
{
    int parent = (child - 1) / 2;
    while (child > 0)
    {
        if (a[child] < a[parent])
        {
            Swap(&a[child], &a[parent]);
            child = parent;
            parent = (child - 1) / 2;
        }
        else
        {
            break;
        }
    }
}
// 向下调整堆
void AdjustDown(int* a, int size, int parent)
{
    int child = parent * 2 + 1;
    while (child < size)
    {
        if (child + 1 < size && a[child + 1] < a[child])
        {
            ++child;
        }
        if (a[child] < a[parent])
        {
            Swap(&a[child], &a[parent]);
            parent = child;
            child = parent * 2 + 1;
        }
        else
        {
            break;
        }
    }
}
// 堆的插入
void HeapPush(Heap* hp, HPDataType  x)
{
    assert(hp);
    if (hp->_size == hp->_capacity)
    {
        int newCapacity = hp->_capacity == 0 ? 4: hp->_capacity * 2;
        HPDataType*temp = (HPDataType*)realloc(hp->_a, newCapacity * sizeof(HPDataType));
        if (temp == NULL)
        {
            perror("realloc fail");
            exit(-1);
        }
        hp->_a =temp;
        hp->_capacity = newCapacity;
    } 
    hp->_a[hp->_size] = x;
    hp->_size++;
    AdjudtUp(hp->_a,hp->_size-1);
}
// 堆的构建
//void HeapCreate(Heap* hp)//HPDataType* a, int n
//{
//    assert(hp);
//    hp->_size = 0; // 初始化堆的大小为0
//    hp->_capacity = 0; // 设置堆的容量为n
//    hp->_a = NULL;
//
//    //// 将数组a中的元素依次插入堆中
//    //for (int i = 0; i < n; i++) {
//    //    HeapPush(&hp, a[i]);
//    //}
//}
void HeapCreate(Heap* hp, HPDataType *a, int n )
{
    assert(hp);
    hp->_size = 0; // 初始化堆的大小为0
    hp->_capacity =NULL; // 
    hp->_a = NULL;

    // 将数组a中的元素依次插入堆中
    for (int i = 0; i < n; i++) {
        HeapPush(hp, a[i]);
    }
}
// 堆的销毁
void HeapDestory(Heap* hp)
{
    assert(hp);
    free(hp->_a);
    hp->_a = NULL;
    hp->_size =hp->_capacity=0;

}
// 堆的删除
void HeapPop(Heap* hp)
{
    assert(hp);
    assert(hp->_size > 0);
    Swap(&hp->_a[0], &hp->_a[hp->_size - 1]);
    hp->_size--;
    AdjustDown(hp->_a, hp->_size, 0);
}
// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
    assert(hp);
    assert(hp->_size > 0);
    return hp->_a[0];
}
// 堆的数据个数
int HeapSize(Heap* hp)
{
    assert(hp);
    return hp->_size;
}
// 堆的判空
int HeapEmpty(Heap* hp)
{
    assert(hp);
    return hp->_size == 0;
}