#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* _a;
	int _size;
	int _capacity;
}Heap;
//数据交换
void Swap(HPDataType* p1, HPDataType* p2);
//向上堆的调整
void AdjudtUp(Heap* _a, int child);
// 向下调整堆
void AdjustDown(int* a, int size, int parent);
// 堆的构建
//void HeapCreate(Heap* hp);//, HPDataType* a, int n
 void HeapCreate(Heap* hp, HPDataType* a, int n);
// 堆的销毁
void HeapDestory(Heap* hp);
// 堆的插入
void HeapPush(Heap* hp, HPDataType x);
// 堆的删除
void HeapPop(Heap* hp);
// 取堆顶的数据
HPDataType HeapTop(Heap* hp);
// 堆的数据个数
int HeapSize(Heap* hp);
// 堆的判空
int HeapEmpty(Heap* hp);