#include"Sort.h"

void TestSelectSort()
{
	int a[] = { 13, 2, 6, 8, 4, 6, 0, 9, 5, 7, 1 };
	SelectSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
}

void TestHeapSort()
{
	int a[] = { 3, 2, 6, 8, 4, 6, 0, 9, 5, 7, 1 };
	

	HeapSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
}

int main()
{
	TestSelectSort();
	//TestHeapSort();
	
	return 0;
}