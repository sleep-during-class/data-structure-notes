#include"SList.h"

void SLTPrint(SLNode* phead)//链表打印
{
	SLNode* cur = phead;//
	while (cur != NULL)
	{
		printf("%d->", cur->val);
		cur = cur->next;
	}
	printf("NULL\n");
}

SLNode* CreateNode(SLNDataType x)   //创作结点
{
	SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	newnode->val = x;
	newnode->next = NULL;
	return newnode;
}

void SLTPushBack(SLNode** pphead, SLNDataType x)//尾插一个链表
//pphead是SLNode*的地址
//改变外部结构体指针SLNode*要用要用二级指针SLNode** 
{
	assert(pphead);

	SLNode* newnode = CreateNode(x);

	if (*pphead == NULL)//*pphead就是plist
	{
		*pphead = newnode;//改变结构体成员
	}
	else
	{
		// 找尾
		SLNode* tail = *pphead;//定义一个* tail的指针
		while (tail->next != NULL)
			//链表里的插入一定是让上一个结点指向下一个结点，存下一个结点的地址
			//找到下一个节点  tail->next != NULL   为了让 tail->next指向newnode(空NULL)
		{
			tail = tail->next;
		}

		tail->next = newnode;
		//改变结构体指针
		//改变外部结构体SLNode*要用要用一级指针SLNode*
	}
}

void SLTPushFront(SLNode** pphead, SLNDataType x)//头插一个链表
{
	assert(pphead);
	SLNode* newnode = CreateNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SLTPopBack(SLNode** pphead)//尾删：时间复杂度为O(N);
{
	// 温柔的检查
	//if (*pphead == NULL)//不能为空
	//	return;
	assert(pphead);
	// 空
	assert(*pphead);

	// 1、一个节点
	// 2、一个以上节点
	if ((*pphead)->next == NULL)//一个节点
	{
		free(*pphead);
		*pphead = NULL;
	}
	//指针指向一个不属于你的空间或者释放的空间就会变成野指针
	else//一个以上节点
	{
		// 找尾——常规型
		/*SLNode* prev = NULL;//找尾的前一个
		SLNode* tail = *pphead;
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		tail = NULL;

		prev->next = NULL;*/

		SLNode* tail = *pphead;
		while (tail->next->next != NULL)//tail指向为最后一个结点的前一个结点
		{
			tail = tail->next;
		}

		free(tail->next);
		tail->next = NULL;
	}
}

void SLTPopFront(SLNode** pphead)//头删
{
	assert(pphead);
	// 空
	assert(*pphead);

	//// 一个以上节点 + 一个
	//SLNode* tmp = (*pphead)->next;
	//free(*pphead);
	//*pphead = tmp;

	// 一个以上节点 + 一个
	SLNode* tmp = *pphead;
	*pphead = (*pphead)->next;//先指向下一个，再释放，防止因为头删使*pphead变为野指针
	free(tmp);
}


SLNode* SLTFind(SLNode* phead, SLNDataType x)//查找
{
	SLNode* cur = phead;
	while (cur)
	{
		if (cur->val == x)
		{
			return cur;
		}
		else
		{
			cur = cur->next;
		}
	}

	return NULL;//如果没有这个节点就返回NULL
}



void SLTInsert(SLNode** pphead, SLNode* pos, SLNDataType x)//pos前面插入
{
	// 严格限定pos一定是链表里面的一个有效节点
	assert(pphead);
	assert(pos);
	assert(*pphead);

	if (*pphead == pos)
	{
		// 头插
		SLTPushFront(pphead, x);
	}
	else//中间插入或者尾插要找pos前一个结点
	{
		SLNode* prev = *pphead;//头结点
		while (prev->next != pos)//找前一个
		{
			prev = prev->next;
		}

		SLNode* newnode = CreateNode(x);
		prev->next = newnode;
		newnode->next = pos;
	}
}

void SLTErase(SLNode** pphead, SLNode* pos)//pos位置前面删除
{
	assert(pphead);
	assert(*pphead);
	assert(pos);

	if (*pphead == pos)
	{
		// 头删
		SLTPopFront(pphead);
	}
	else//中间插入或者尾插要找pos前一个结点
	{
		SLNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		prev->next = pos->next;
		free(pos);
		pos = NULL;
	}
}


void SLTInsertAfter(SLNode* pos, SLNDataType x)//pos位置后面插入

{
	assert(pos);
	SLNode* newnode = CreateNode(x);

	newnode->next = pos->next;//注意顺序
	pos->next = newnode;
}

void SLTEraseAfter(SLNode* pos)//pos位置下一个节点删除
{
	assert(pos);
	assert(pos->next);

	SLNode* tmp = pos->next;//注意顺序
	pos->next = pos->next->next;

	free(tmp);
	tmp = NULL;
}


void SLTDestroy(SLNode** pphead)//清空所有节点
{
	assert(pphead);

	SLNode* cur = *pphead;
	while (cur)
	{
		SLNode* next = cur->next;
		free(cur);
		cur = next;
	}

	*pphead = NULL;
}


//当前作用域的变量出了作用域就会自动销毁
//
//                         类型
//pphead                 SLNode**             对应的是外部的plist的地址，地址不可能为空，为空就是错误
//*pphead                SLNode*               对应的是外部的plist,若为空代表没有节点的空链表
//(*pphead)->next         SLNode*               若为空代表只有一个节点的链表
//
//
//
//
